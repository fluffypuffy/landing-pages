# Landing Pages Collection

This repository contains a collection of three distinct landing page projects. Each project showcases different design approaches and functionalities suited for various purposes. Below is a brief overview of the included projects:

- **Trafalgar**: A healthcare-related landing page emphasizing clean design and straightforward navigation to offer services and information effectively.
- **Weiss**: This project presents a more minimalist approach, focusing on high-end products or services with a sleek and modern aesthetic.
- **UDIX**: Geared towards digital solutions, this landing page combines bold visuals with interactive elements to engage visitors and showcase technology services.

Each project folder contains all the necessary assets and code files to deploy or further develop the landing pages.

## Usage

To view a project, simply open the `index.html` file in your preferred web browser.